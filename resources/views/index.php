<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />	
	<title>React - Bootstrap Theme</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />

	<!-- stylesheets -->
	<link rel="stylesheet" type="text/css" href="css/compiled/theme.css">
	<link rel="stylesheet" type="text/css" href="css/vendor/animate.css">

	<!-- javascript -->
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
	<script src="js/bootstrap/bootstrap.min.js"></script>
	<script src="js/theme.js"></script>

	<!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
</head>
<body id="home">
	<header class="navbar navbar-inverse hero" role="banner">
  		<div class="container">
    		<div class="navbar-header">
		      	<button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".bs-navbar-collapse">
			        <span class="icon-bar"></span>
			        <span class="icon-bar"></span>
			        <span class="icon-bar"></span>
		      	</button>
      			<a href="index.html" class="navbar-brand">STUDENT MANAGEMENT</a>
    		</div>
    		
    		</nav>
  		</div>
	</header>

	<div id="hero">
		<div class="container">
			<h1 class="hero-text animated fadeInDown">
				The best management system
			</h1>
			<p class="sub-text animated fadeInDown">
				Get a complete mangement system up and running in no time.
			</p>
			<div class="cta animated fadeInDown">
				<a href="features.html" class="button-outline">list students</a>
				<a href="signup.html" class="button">add new</a>
			</div>
		
		</div>
	</div>

	

	<div id="pricing">
		<div class="container">
			<div class="row header">
				<div class="col-md-12">
					<h3>list of students.</h3>
					
				</div>
			</div>
			<div class="row charts">
				<div class="col-md-3">
					<div class="chart first">
						
						<div class="plan-name">sumit</div>
						<div class="specs">
							<div class="spec">
								<span class="variable"></span>
								team members
							</div>
							<div class="spec">
								<span class="variable">Digital</span>
								recurring billing
							</div>
							<div class="spec">
								<span class="variable">Virtual</span>
								online terminal
							</div>
							<div class="spec">
								<span class="variable">10</span>
								total products
							</div>
							<div class="spec">
								<span class="variable">1.0%</span>
								Transaction fee
							</div>
						</div>
						<a class="btn-signup button-clear" href="signup.html">
							<span>view</span>
						</a>
						<a class="btn-signup button-clear" href="signup.html">
							<span>edit</span>
						</a>
					</div>
				</div>
				<div class="col-md-3">
					<div class="chart first">
						
						<div class="plan-name">savita</div>
						<div class="specs">
							<div class="spec">
								<span class="variable">15</span>
								team members
							</div>
							<div class="spec">
								<span class="variable">Digital</span>
								recurring billing
							</div>
							<div class="spec">
								<span class="variable">Virtual</span>
								online terminal
							</div>
							<div class="spec">
								<span class="variable">15</span>
								total products
							</div>
							<div class="spec">
								<span class="variable">0.5%</span>
								Transaction fee
							</div>
						</div>
						<a class="btn-signup button-clear" href="signup.html">
							<span>view</span>
						</a>
						<a class="btn-signup button-clear" href="signup.html">
							<span>edit</span>
						</a>
					</div>
				</div>
				<div class="col-md-3">
					<div class="chart first">
						
						<div class="plan-name">savita</div>
						<div class="specs">
							<div class="spec">
								<span class="variable">15</span>
								team members
							</div>
							<div class="spec">
								<span class="variable">Digital</span>
								recurring billing
							</div>
							<div class="spec">
								<span class="variable">Virtual</span>
								online terminal
							</div>
							<div class="spec">
								<span class="variable">15</span>
								total products
							</div>
							<div class="spec">
								<span class="variable">0.5%</span>
								Transaction fee
							</div>
						</div>
						<a class="btn-signup button-clear" href="signup.html">
							<span>view</span>
						</a>
						<a class="btn-signup button-clear" href="signup.html">
							<span>edit</span>
						</a>
					</div>
				</div>
				<div class="col-md-3">
					<div class="chart first">
						
						<div class="plan-name">sameer</div>
						<div class="specs">
							<div class="spec">
								<span class="variable">Unlimited</span>
								team members
							</div>
							<div class="spec">
								<span class="variable">Digital</span>
								recurring billing
							</div>
							<div class="spec">
								<span class="variable">Virtual</span>
								online terminal
							</div>
							<div class="spec">
								<span class="variable">25</span>
								total products
							</div>
							<div class="spec">
								<span class="variable">No</span>
								Transaction fee
							</div>
						</div>
						<a class="btn-signup button-clear" href="signup.html">
							<span>view</span>
						</a>
						<a class="btn-signup button-clear" href="signup.html">
							<span>edit</span>
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>

	
	

	

	<div id="footer-white">
		<div class="container">
			
				
				
				
			<div class="row credits">
				<div class="col-md-12">
					Copyright © 2014. ankit chauhan
				</div>
			</div>
		
	</div>
</body>
</html>